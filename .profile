#!/bin/sh
#
# ~/.profile
#
# This file is not sourced by Bash if ~/.bash_profile exists. We do have ~/.bash_profile, so we will
# source this file from there. We will also source this file from our $ZDOTDIR/.zshenv startup file,
# to handle that shell's login case. This file is sourced for all login shells, and is for setting
# up the initial environment for all shells, even non-interactive, non-login shells inheriting from
# this environment, such as cron-invoked scripts. Therefor, we so we do not want things like
# coloring, altering of default program behavior, or anything else only useful in an interactive
# shell context. Also, we do not want to use any syntax or features in this file that are not
# Bourne-shell compatible.

# If we run Bourne shell, it sources for the file defined in this variable.
export ENV="$HOME/.shrc"

# I use these XDG variables in many scripts, so we set them once for all shells here.
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.data"

# Store the original PATH variable, because we want to be able to modify it for each interactive
# shell session, so we will build off of the original path.
export ORIGINAL_PATH="$PATH"

