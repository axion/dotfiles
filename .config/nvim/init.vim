" Plugins {{{
call plug#begin('~/.data/nvim/plugins')
Plug 'nvim-lua/popup.nvim' " Pop-up window API
Plug 'nvim-lua/plenary.nvim' " Utility functions
Plug 'mattn/webapi-vim' " Web API support required by some plugins
Plug 'nvim-telescope/telescope.nvim' " UI for finding things
Plug 'nvim-telescope/telescope-fzf-native.nvim', { 'do': 'gmake' } " telescope fzf sorter
Plug 'nvim-telescope/telescope-project.nvim' " Telescope extension for finding projects
Plug 'airblade/vim-rooter' " Sets working directory to project root when opening files
Plug 'ntpeters/vim-better-whitespace' "  Strip end of line/file whitespace when saving a file
Plug 'moll/vim-bbye' " Delete buffers without changing window layout
Plug 'farmergreg/vim-lastplace' " Remember cursor position in file across Vim instances
Plug 'wellle/targets.vim' " Additional Vim text objects
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'} " Language parsing
Plug 'ryanoasis/vim-devicons' " Icons
Plug 'kyazdani42/nvim-web-devicons' " Icons

" Use fork until PR #285 is merged
" Plug 'akinsho/bufferline.nvim' " Tab line
Plug 'kazhala/bufferline.nvim' " Tab line

Plug 'machakann/vim-highlightedyank' " Highlight yanks
Plug 'NTBBloodbath/galaxyline.nvim' " Status line
Plug 'kyazdani42/nvim-tree.lua' " File browser
Plug 'navarasu/onedark.nvim' " Color theme
Plug 'lewis6991/gitsigns.nvim' " Show git change indicators in the sign column
Plug 'f-person/git-blame.nvim' " Show git blame message for current line
Plug 'psliwka/vim-smoothie' " Smooth scrolling
Plug 'unblevable/quick-scope' " Highlight letters to jump to when starting a f, F, t, or T motion
Plug 'machakann/vim-highlightedyank' " Flash regions when yanking text
Plug 'w0rp/ale' " Code linting engine
Plug 'kshenoy/vim-signature' " Show Vim mark names in the sign column
Plug 'lukas-reineke/indent-blankline.nvim' " Show indentation guides
Plug 'tpope/vim-repeat' " Support for repeating more commands
Plug 'tpope/vim-surround' " Bindings for surrounding text
Plug 'chaoren/vim-wordmotion' " Extend Vim's definition of a 'word' to camel-case and more
Plug 'liuchengxu/vim-which-key' " Key binding guide
Plug 'rust-lang/rust.vim' " Language support: Rust
Plug 'racer-rust/vim-racer' " Language support: Rust
Plug 'timonv/vim-cargo' " Language support: Rust
Plug 'cespare/vim-toml' " Language support: TOML
Plug 'maralla/vim-toml-enhance' " Language support: TOML
Plug 'plasticboy/vim-markdown' " Language support: Markdown
Plug 'rhysd/vim-gfm-syntax' " Language support GitHub-flavored Markdown
Plug 'JuliaEditorSupport/julia-vim' " Language support: Julia
Plug 'elzr/vim-json' " Language support: JSON
Plug 'iamcco/vim-language-server' " Language support: Vim script
Plug 'bash-lsp/bash-language-server' " Language support: Bash
Plug 'itspriddle/vim-shellcheck' " Language support: Shell
Plug 'othree/html5.vim' " Language support: HTML
Plug 'alvan/vim-closetag' " Automatically close HTML tags
Plug 'othree/csscomplete.vim' " Language support: CSS
Plug 'neovim/nvim-lspconfig' " Language Server Protocol support
Plug 'tjdevries/lsp_extensions.nvim' " Language Server Protocol support
Plug 'mfussenegger/nvim-dap' " Debug Adapter Protocol support
Plug 'b3nj5m1n/kommentary' " Code commenting
Plug 'junegunn/vim-peekaboo' " Vim register guide
Plug 'mattn/gist-vim' " Allow publishing GitHub gists of current buffer/region
Plug 'haya14busa/incsearch.vim' " Incremental search
Plug 'haya14busa/incsearch-fuzzy.vim' " Incremental search
Plug 'tpope/vim-eunuch' " Vim commands for UNIX shell commands
Plug 'tpope/vim-fugitive' " Git integration
Plug 'tpope/vim-rhubarb' " GitHub integration
Plug 'phaazon/hop.nvim' " Quickly jump to any word or line in a buffer
Plug 'hrsh7th/nvim-cmp' " Completion system
Plug 'hrsh7th/cmp-nvim-lsp' " Completion system extension
Plug 'hrsh7th/cmp-buffer' " Completion system extension
Plug 'hrsh7th/cmp-path' " Completion system extension
Plug 'hrsh7th/cmp-cmdline' " Completion system extension
Plug 'voldikss/vim-floaterm' " Interact with floating windows
Plug 'mhinz/vim-startify' " Start screen
Plug 't9md/vim-choosewin' " Jump to/swap windows intuitively
Plug 'SuneelFreimuth/vim-gemtext' " Gemini gemtext syntax support
Plug 'fatih/vim-go' " Go language support
Plug 'leoluz/nvim-dap-go' " Go language support
call plug#end()
" }}}
" Neovim options {{{
set autochdir
set autoread
set backup
set backupdir=$XDG_CACHE_HOME/nvim/backup//
set clipboard=unnamedplus
set colorcolumn=+1
set complete+=k
set completeopt=noinsert,menuone,noselect
set cursorline
set dictionary=/usr/share/dict/words
set directory=$XDG_CACHE_HOME/nvim//swap//
set expandtab
set fileencoding=utf-8
set fillchars=vert:\┃,diff:•,fold:-
set foldlevelstart=0
set foldopen-=block
set formatoptions=tcqnj
set gdefault
set hidden
set ignorecase
set iskeyword+=-
set lazyredraw
set matchpairs+=<:>
set matchtime=3
set modelines=0
set mouse=a
set noshowmode
set noswapfile
set nowrap
set number
set pumheight=10
set scrolloff=3
set shiftround
set shiftwidth=2
set shortmess+=cI
set showbreak=↪
set showmatch
set sidescrolloff=4
set signcolumn=yes
set smartcase
set smartindent
set smarttab
set softtabstop=2
set splitbelow
set splitright
set synmaxcol=200
set tabstop=2
set termguicolors
set textwidth=100
set timeoutlen=350
set title
set undodir=$XDG_CACHE_HOME/nvim/undo//
set undofile
set updatetime=300
set visualbell
set wildignore+=*.swp,*~,.tmp,__pycache__/
set wildignore+=.git/,.hg/,.svn/
set wildignore+=*.exe,*.bin,*.dll,*.o,*.so
set wildignore+=*.elc,*.fasl,*.dx64fsl,*.lx64fsl,*.pyc,*.luac
set wildignore+=*.jpg,*.jpeg,*.bmp,*.gif,*.png,*.tiff,*.tga
set wildignore+=*.7z,*.bz2,*.dmg,*.gz,*.iso,*.jar,*.rar,*.tar,*.tgz,*.xz,*.zip
set wildignore+=.DS_Store,Thumbs.db
set wildignore+=*.sql,*.sqlite,*.db
set wildignore+=*/target/*
set wildmode=list:full
" }}}
" Create needed directories {{{
if !isdirectory(expand(&undodir))
  call mkdir(expand(&undodir), "p")
endif
if !isdirectory(expand(&backupdir))
  call mkdir(expand(&backupdir), "p")
endif
if !isdirectory(expand(&directory))
  call mkdir(expand(&directory), "p")
endif
" }}}
" Abbreviations {{{
iabbrev todo TODO:
iabbrev _mail mail@mfiano.net
iabbrev _sig Michael Fiano <mail@mfiano.net>
iabbrev _web https://mfiano.net
iabbrev <expr> _date strftime("%Y-%m-%d %H:%M:%S")
" }}}
" Color theme {{{
colorscheme onedark
" }}}
augroup vim_options " {{{
  au!
  au vimleave * set guicursor=a:hor25
  au bufreadpost quickfix nnoremap <buffer><cr> <cr>
  au winleave,insertenter * set nocursorline
  au winenter,insertleave * set cursorline
  au vimresized * :wincmd =
  au insertenter * :set listchars-=trail:⌴
  au insertleave * :set listchars+=trail:⌴
  au termenter * :set nonumber
augroup end
" }}}

augroup ft_commonlisp " {{{
  au!
  au bufread,bufnewfile *.asd,*.ros setfiletype lisp
  au filetype lisp hi link lispKey Keyword
  au filetype lisp setlocal nolisp
augroup end " }}}
augroup ft_css " {{{
  au!
  au filetype css setlocal omnifunc=csscomplete#CompleteCSS noci
augroup end " }}}
augroup ft_diff " {{{
  au!
  au filetype diff setlocal foldmethod=expr
  au filetype diff setlocal foldexpr=DiffFoldLevel()
augroup end " }}}
augroup ft_glsl " {{{
  au!
  au filetype glsl setlocal foldmethod=marker foldmarker={,}
augroup end " }}}
augroup ft_go " {{{
  au!
  lua require('ft.go')
  lua require('dap-go').setup()
  au bufwritepre (insertleave?) <buffer> lua vim.lsp.buf.formatting_sync(nil,500)
  au filetype go setl noet ts=2 sw=2 sts=2
  au filetype go nnoremap <buffer> <localleader>; :FloatermNew --name=go --title=go\ $1/$2<cr>
  au filetype go nnoremap <buffer> <localleader>b :GoBuild -race -trimpath<cr>
  au filetype go nnoremap <buffer> <localleader>l :GoMetaLinter<cr>
  au filetype go nnoremap <buffer> <localleader>r :GoRun<cr>
  au filetype go nnoremap <buffer> <localleader>R :GoRun %<cr>
  au filetype go nnoremap <buffer> <localleader>t :GoTest<cr>
  au filetype go nnoremap <buffer> <localleader>T :GoTestFunc<cr>
  au filetype go nnoremap <buffer> <localleader>gb :GoDefPop<cr>
  au filetype go nnoremap <buffer> <localleader>gd :GoDef<cr>
  au filetype go nnoremap <buffer> <localleader>gt :GoDefType<cr>
  au filetype go nnoremap <buffer> <localleader>ic :GoCallees<cr>
  au filetype go nnoremap <buffer> <localleader>iC :GoCallers<cr>
  au filetype go nnoremap <buffer> <localleader>id :GoDescribe<cr>
  au filetype go nnoremap <buffer> <localleader>ii :GoImplements<cr>
  au filetype go nnoremap <buffer> <localleader>is :GoCallstack<cr>
augroup end " }}}
augroup ft_gemtext " {{{
  au!
  au filetype gemtext setlocal wrap lbr tw=0 wm=0
augroup end " }}}
augroup ft_html " {{{
  au!
  au filetype html setlocal foldmethod=indent
augroup end " }}}
augroup ft_json " {{{
  au!
  au filetype json setlocal foldmethod=syntax
augroup end " }}}
augroup ft_julia " {{{
  au!
  lua require'lspconfig'.julials.setup{}
  au filetype julia nnoremap <buffer> <localleader>;
        \ :FloatermNew --name=julia --title=julia\ $1/$2<cr>
augroup end " }}}
augroup ft_markdown " {{{
  au!
  au bufnewfile,bufread *.md setlocal filetype=markdown foldlevel=1
augroup end " }}}
augroup ft_rust " {{{
  au!
  lua require'lspconfig'.rust_analyzer.setup{}
  au filetype rust nnoremap <localleader>f :RustFmt<cr>
  au filetype rust nnoremap <buffer> <localleader>;
        \ :FloatermNew --name=rust --title=rust\ $1/$2<cr>
  au filetype rust nnoremap <buffer> <localleader>b
        \ :FloatermSend --name=rust cargo build<cr>
        \:FloatermHide!<cr>
        \:FloatermShow rust<cr>
  au filetype rust nnoremap <buffer> <localleader>B
        \ :FloatermSend --name=rust cargo build --release<cr>
        \:FloatermHide!<cr>
        \:FloatermShow rust<cr>
  au filetype rust nnoremap <buffer> <localleader>c
        \ :FloatermSend --name=rust cargo check<cr>
        \:FloatermHide!<cr>
        \:FloatermShow rust<cr>
  au filetype rust nnoremap <buffer> <localleader>r
        \ :FloatermSend --name=rust cargo run<cr>
        \:FloatermHide!<cr>
        \:FloatermShow rust<cr>
  au filetype rust nnoremap <buffer> <localleader>R
        \ :FloatermSend --name=rust cargo run --release<cr>
        \:FloatermHide!<cr>
        \:FloatermShow rust<cr>
  au filetype rust nnoremap <buffer> <localleader>t
        \ :FloatermSend --name=rust cargo test<cr>
        \:FloatermHide!<cr>
        \:FloatermShow rust<cr>
augroup end " }}}
augroup ft_shell " {{{
  au!
  lua require'lspconfig'.bashls.setup{}
  let g:sh_fold_enabled=3
  au filetype sh nnoremap <buffer> <localleader>c :ShellCheck!<cr>
  au filetype sh nnoremap <buffer> <localleader>;
        \ :FloatermNew --name=shell --title=shell\ $1/$2<cr>
augroup end " }}}
augroup ft_yaml " {{{
  au!
  au filetype yaml setlocal indentkeys=<:>
augroup end " }}}
augroup ft_vim " {{{
  au!
  lua require'lspconfig'.vimls.setup{}
  au filetype vim setlocal foldmethod=marker spell
  au filetype vim setlocal fo-=o fo-=r
  au filetype vim nnoremap <buffer> <localleader>s :source $MYVIMRC<cr>
augroup end " }}}

fun! SmartHome() " {{{
  let first_nonblank = match(getline('.'), '\S') + 1
  if first_nonblank == 0
    return col('.') + 1 >= col('$') ? '0' : '^'
  endif
  if col('.') == first_nonblank
    return '0'
  endif
  return &wrap && wincol() > 1 ? 'g^' : '^'
endfun " }}}

" ale {{{
let g:ale_rust_cargo_use_clippy = executable('cargo-clippy')
let g:ale_rust_rls_config = { 'rust': { 'clippy_preference': 'on' } }
" }}}
" bufferline {{{
lua require('plugin.bufferline')
" }}}
" gist-vim {{{
let g:gist_clip_command = 'xclip -selection primary'
let g:gist_detect_filetype = 1
let g:gist_post_private = 1
let g:gist_open_browser_after_post = 1
" }}}
" git-blame {{{
let g:gitblame_enabled = 0
" }}}
" galaxyline {{{
lua require('plugin.galaxyline')
" }}}
" gitsigns {{{
lua require('plugin.gitsigns')
" }}}
" hop {{{
lua require'hop'.setup()
" }}}
" incsearch {{{
let g:incsearch#auto_nohlsearch = 1
" }}}
" indent-blankline {{{
let g:indent_blankline_enabled = v:false
let g:indent_blankline_char = '┊'
let g:indent_blankline_use_treesitter = v:true
" }}}
" kommentary {{{
lua require('plugin.kommentary')
" }}}
" nvim-cmp {{{
lua require('plugin.nvim-cmp')
" }}}
" nvim-tree {{{
lua require('plugin.nvim-tree')
let g:nvim_tree_quit_on_open = 1
let g:nvim_tree_show_icons = {
    \ 'git': 0,
    \ 'folders': 1,
    \ 'files': 1,
    \ }
" }}}
" nvim-treesitter {{{
lua require('plugin.nvim-treesitter')
" }}}
" quick-scope {{{
let g:qs_highlight_on_keys = ['f', 'F', 't', 'T']
" }}}
" rust.vim {{{
let g:rustfmt_autosave = 1
let g:rustfmt_command = 'rustup run nightly rustfmt'
" }}}
" telescope {{{
lua require('plugin.telescope')
" }}}
" telescope-fzf-native {{{
lua require'telescope'.load_extension('fzf')
" }}}
" telescope-project {{{
lua require'telescope'.load_extension('project')
" }}}
" vim-better-whitespace {{{
let g:better_whitespace_enabled=0
let g:strip_whitespace_on_save=1
let g:strip_whitespace_confirm=0
let g:strip_whitelines_at_eof=1
" }}}
" vim-floaterm {{{
let g:floaterm_wintype = "split"
let g:floaterm_width = 1.0
let g:floaterm_height = 0.25
let g:floaterm_position = "bottom"
let g:floaterm_autoclose = 1
let g:floaterm_autohide = 1
let g:floaterm_borderchars=""
"  }}}
" vim-go {{{
let g:go_auto_type_info = 0
let g:go_def_mapping_enabled = 0
let g:go_doc_popup_window = 1
let g:go_fmt_command = 'goimports'
let g:go_highlight_build_constraints = 1
let g:go_highlight_extra_types = 1
let g:go_highlight_fields = 1
let g:go_highlight_functions = 1
let g:go_highlight_function_calls = 1
let g:go_highlight_function_parameters = 1
let g:go_highlight_operators = 1
let g:go_highlight_types = 1
let g:go_highlight_variable_assignments = 1
let g:go_highlight_variable_declarations = 1
let g:go_imports_mode = 'goimports'
let g:go_list_autoclose = 1
let g:go_list_height = 10
let g:go_metalinter_autosave = 0
let g:go_metalinter_autosave_enabled = []
let g:go_metalinter_command = 'golangci-lint'
let g:go_metalinter_enabled = []
let g:go_template_use_pkg = 1
let g:go_term_enabled = 1
let g:go_term_mode = "split"
let g:go_term_reuse = 1
let g:go_test_show_name = 1
let g:go_updatetime = 0
" }}}
" vim-highlightedyank {{{
let g:highlightedyank_highlight_duration=250
hi HighlightedyankRegion cterm=reverse gui=reverse
" }}}
" vim-markdown {{{
let g:vim_markdown_folding_disabled = 1
let g:vim_markdown_conceal = 0
let g:vim_markdown_new_list_item_indent = 0
let g:vim_markdown_auto_insert_bullets = 0
" }}}
" vim-racer {{{
let g:racer_cmd = '~/.cargo/bin/racer'
let g:racer_experimental_completer = 1
" }}}
" vim-rooter {{{
let g:rooter_silent_chdir = 1
" }}}
" vim-which-key {{{
let g:which_key_timeout = 100
let g:which_key_display_names = { '<cr>': '↵', '<space>': 'SPC' }
let g:which_key_sep = '➜ '
let g:which_key_use_floating_win = 0
let g:which_key_max_size = 0
let g:which_key_disable_default_offset = 1
let g:which_key_centered = 0
let g:which_key_map =  {}
let g:which_key_map.b = { 'name': '+buffer' }
let g:which_key_map.b.b = 'switch'
let g:which_key_map.b.d = 'delete'
let g:which_key_map.e = { 'name': '+edit' }
let g:which_key_map.e.l = 'jump to line'
let g:which_key_map.e.w = 'jump to word'
let g:which_key_map.f = { 'name': '+file' }
let g:which_key_map.f.d = 'delete file'
let g:which_key_map.f.f = 'find file'
let g:which_key_map.f.h = 'find in home'
let g:which_key_map.f.r = 'find recent'
let g:which_key_map.f.R = 'rename'
let g:which_key_map.f.s = 'save'
let g:which_key_map.f.S = 'sudo save'
let g:which_key_map.f.t = 'toggle file tree'
let g:which_key_map.f.v = 'open vim config'
let g:which_key_map.g = { 'name': '+git' }
let g:which_key_map.g.b = 'toggle blame'
let g:which_key_map.g.c = 'commits'
let g:which_key_map.g.C = 'buffer commits'
let g:which_key_map.g.g = 'gist'
let g:which_key_map.g.G = 'gist private'
let g:which_key_map.g.w = 'git web browse'
let g:which_key_map.l = { 'name': '+language' }
let g:which_key_map.l.d = 'definition'
let g:which_key_map.p = { 'name': '+project' }
let g:which_key_map.p.f = 'find in project'
let g:which_key_map.p.p = 'switch project'
let g:which_key_map.s = { 'name': '+search' }
let g:which_key_map.s[';'] = 'command history'
let g:which_key_map.s.b = 'search buffer'
let g:which_key_map.s.d = 'search directory'
let g:which_key_map.s.m = 'marks'
let g:which_key_map.s.p = 'search project'
let g:which_key_map.s.s = 'spelling'
let g:which_key_map.t = { 'name': '+toggle' }
let g:which_key_map.t.h = 'search highlight'
let g:which_key_map.t.i = 'indent guides'
let g:which_key_map.t.n = 'line numbers'
let g:which_key_map.v = { 'name': '+vim' }
let g:which_key_map.v.c = 'packages clean'
let g:which_key_map.v.i = 'packages install'
let g:which_key_map.v.s = 'packages status'
let g:which_key_map.v.u = 'packages update'
let g:which_key_map.v.U = 'package manager upgrade'
let g:which_key_map.w = { 'name': '+window' }
let g:which_key_map.w['-'] = 'split horizontal'
let g:which_key_map.w['|'] = 'split vertical'
let g:which_key_map.w['='] = 'rebalance'
let g:which_key_map.w.d = 'delete'
let g:which_key_map.w.s = 'swap'
call which_key#register('<space>', "g:which_key_map")

augroup which_key
  au! filetype which_key
  au filetype which_key set laststatus=0 noruler | au bufleave <buffer> set laststatus=2 ruler
augroup end
" }}}

" Global keybindings {{{

" Allow entering commands quicker with ; instead of :
nnoremap ; :

" Disable help key
noremap <f1> <nop>
inoremap <f1> <nop>

" Close a window quickly with Q
nnoremap Q :q<cr>

" Yank to end of line with Y instead of y$
nnoremap Y y$

" Buffer bar management
nnoremap <a-[> :BufferLineCyclePrev<cr>
nnoremap <a-]> :BufferLineCycleNext<cr>
nnoremap <a-t> :tabnew<cr>
nnoremap <a-x> :tabclose<cr>
nnoremap <a-1> 1gt
nnoremap <a-2> 2gt
nnoremap <a-3> 3gt
nnoremap <a-4> 4gt
nnoremap <a-5> 5gt
nnoremap <a-6> 6gt
nnoremap <a-7> 7gt
nnoremap <a-8> 8gt
nnoremap <a-9> 9gt

" Insert a new line when pressing Return in normal mode
nnoremap <cr> o<esc>

" Switch windows with Shift + arrow keys
nnoremap <s-up> <c-w><c-k>
nnoremap <s-down> <c-w><c-j>
nnoremap <s-left> <c-w><c-h>
nnoremap <s-right> <c-w><c-l>
tnoremap <s-up> <c-\><c-n><c-w><c-k>
tnoremap <s-down> <c-\><c-n><c-w><c-j>
tnoremap <s-left> <c-\><c-n><c-w><c-h>
tnoremap <s-right> <c-\><c-n><c-w><c-l>

" Better indenting of visual selection
vnoremap < <gv
vnoremap > >gv

" Move selected lines with Alt + Up/Down
xnoremap <a-up> :move '<-2<cr>gv-gv
xnoremap <a-down> :move '>+1<cr>gv-gv

" Equalize window sizes
nnoremap - :wincmd =<cr>

" Alternate between first column and first non-blank character of the line with the Home key
noremap <expr> <silent> <home> SmartHome()
imap <silent> <home> <c-o><home>

" Open folds with Tab
nnoremap <tab> za
vnoremap <tab> za

" Leader keys
let g:mapleader = ' '
let g:maplocalleader = ","
nnoremap <space> <nop>
nnoremap <silent> <leader> :silent <c-u> :silent WhichKey '<space>'<cr>
vnoremap <silent> <leader> :silent <c-u> :silent WhichKeyVisual '<space>'<cr>
nnoremap <silent> <localleader> :silent <c-u> :silent WhichKey ','<cr>
vnoremap <silent> <localleader> :silent <c-u> :silent WhichKeyVisual ','<cr>

" Incremental searching
map / <plug>(incsearch-forward)
map ? <plug>(incsearch-backward)
map n <plug>(incsearch-nohl-n)
map N <plug>(incsearch-nohl-N)

" Toggle terminal
nnoremap <silent> <a-tab> :FloatermToggle<cr>
tnoremap <silent> <a-tab> <c-\><c-n>:FloatermToggle<cr>

" Enter normal mode easier in the terminal
tnoremap <f13> <c-\><c-n>
" }}}
" Leader keybindings {{{
nnoremap <leader>bb :Telescope buffers<cr>
nnoremap <leader>bd :Bwipeout<cr>
nnoremap <leader>el :HopLine<cr>
nnoremap <leader>ew :HopWord<cr>
nnoremap <leader>fd :Delete<cr>
nnoremap <leader>ff :Telescope find_files hidden=true<cr>
nnoremap <leader>fh :Telescope find_files cwd=~ hidden=true<cr>
nnoremap <leader>fr :Telescope oldfiles<cr>
nnoremap <leader>fR :Rename<space>
nnoremap <leader>fs :w!<cr>
nnoremap <leader>fS :SudoWrite<cr>
nnoremap <leader>ft :NvimTreeToggle<cr>
nnoremap <leader>fv :e $MYVIMRC<cr>
nnoremap <leader>gb :GitBlameToggle<cr>
nnoremap <leader>gc :Telescope git_commits<cr>
nnoremap <leader>gC :Telescope git_bcommits<cr>
nnoremap <leader>gg :Gist! -P<cr>
vnoremap <leader>gg :Gist! -P<cr>
nnoremap <leader>gG :Gist! -p<cr>
vnoremap <leader>gG :Gist! -p<cr>
nnoremap <leader>gw :Gbrowse<cr>
nnoremap <leader>ld <cmd>lua vim.lsp.buf.definition()<cr>
nnoremap <leader>pf :Telescope git_files<cr>
nnoremap <leader>pp :Telescope project<cr>
nnoremap <leader>s; :Telescope command_history<cr>
nnoremap <leader>sb :Telescope current_buffer_fuzzy_find<cr>
nnoremap <expr> <leader>sd ':Telescope live_grep cwd='.expand('%:p:h').'/<cr>'
nnoremap <leader>sm :Telescope marks<cr>
nnoremap <leader>sp :Telescope live_grep<cr>
nnoremap <leader>ss :Telescope spell_suggest<cr>
nnoremap <leader>th :nohls<cr>
nnoremap <leader>ti :IndentBlanklineToggle<cr>
nnoremap <leader>tn :setl number!<cr>
nnoremap <leader>vc :PlugClean<cr>
nnoremap <leader>vi :PlugInstall<cr>
nnoremap <leader>vs :PlugStatus<cr>
nnoremap <leader>vu :PlugUpdate<cr>
nnoremap <leader>vU :PlugUpgrade<cr>
nnoremap <leader>w- :split<cr>
nnoremap <leader>w\| :vsplit<cr>
nnoremap <leader>w= <c-w>=
nnoremap <leader>wd :close<cr>
nmap <leader>ws <plug>(choosewin)
" }}}
