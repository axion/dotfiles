lspconfig = require "lspconfig"
lspconfig.gopls.setup {
  settings = {
    gopls = {
      analyses = {
        fieldalignment = true,
        nilness = true,
        unusedparams = true,
      },

      staticcheck = true,
    },
  },
}
