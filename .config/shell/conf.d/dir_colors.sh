#!/usr/bin/env bash

if [ -f "$XDG_CONFIG_HOME/dir_colors" ]; then
  eval "$(dircolors -b "$XDG_CONFIG_HOME/dir_colors")"
fi
