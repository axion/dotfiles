#!/usr/bin/env bash
#
# ~/.bashrc
#
# This file is sourced by interactive Bash shells.

# If this isn't an interactive shell, don't source the rest of this file.
[[ $- = *i* ]] || return

# If a command isn't in the hash table, perform a full path search instead. This prevents the need
# to manually rehash with `hash -r` when an executable moves.
shopt -s checkhash

# Combine multiple line commands into the same history entry.
shopt -s cmdhist

# Path completion includes results beginning with a dot.
shopt -s dotglob

# Enable extended pattern matching for path completion.
shopt -s extglob

# Allow the use of ** in path completion to expand to all files and directories.
shopt -s globstar

# Append to the history file on exit, rather than replacing it entirely.
shopt -s histappend

# Don't try to complete a path on an empty line.
shopt -s no_empty_cmd_completion

# Perform path completion in a case-insensitive fashion.
shopt -s nocaseglob

# Do not echo control characters such as ^C visually.
stty -ctlecho

# Do not handle output control with Ctrl-s and Ctrl-q.
stty -ixon -ixoff

# 'readline' options.
bind "set bell-style none"
bind "set blink-matching-paren on"
bind "set colored-completion-prefix on"
bind "set colored-stats on"
bind "set completion-ignore-case on"
bind "set completion-map-case on"
bind "set mark-directories on"
bind "set mark-symlinked-directories on"
bind "set show-all-if-ambiguous on"
bind "set show-all-if-unmodified on"
bind '"\e[A": history-search-backward'
bind '"\e[B": history-search-forward'
bind '"\e[C": forward-char'
bind '"\e[D": backward-char'
bind '"\e[1~": beginning-of-line'
bind '"\e[4~": end-of-line'

# Create the history directory if it doesn't exist.
HIST_DIR="$XDG_CACHE_HOME/bash"
[[ -d "$HIST_DIR" ]] || mkdir -p "$HIST_DIR"

# History settings.
export HISTCONTROL="erasedups:ignoreboth"
export HISTFILE="$HIST_DIR/history"
export HISTSIZE=100000
export SAVEHIST=100000

# Configure the prompt.
export PS1='\[\e[01;31m\]\u\[\e[m\]@\[\e[01;34m\]\h\[\e[00m\] \w\$ '

# For long paths in the shell prompt, replace all but the last 2 directories with an ellipsis.
export PROMPT_DIRTRIM=2

# Source the file providing interactive environment configuration of all shell variants.
source "$HOME/.config/shell/env"

# Source the file providing Bash-specific environment configuration.
source "$HOME/.config/bash/env"

