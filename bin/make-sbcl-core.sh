#!/bin/sh

rm -f "$XDG_DATA_HOME/sbcl/sbcl.core"

SBCL_HOME="" rlwrap-lisp.sh sbcl \
  --sysinit "$XDG_DATA_HOME/sbcl/sbclrc" \
  --eval '(uiop:call-image-dump-hook)' \
  --eval '(sb-ext:save-lisp-and-die "/home/mfiano/.data/sbcl/sbcl.core")'
