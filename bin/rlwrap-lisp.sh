#!/bin/sh

rlwrap -m$$$$ \
  --multi-line-ext .lisp \
  -a___ \
  -p'1;31' \
  --quote-characters '"' \
  --histsize 1000 \
  --history-filename "$XDG_DATA_HOME/rlwrap/lisp_history" \
  "$@"
