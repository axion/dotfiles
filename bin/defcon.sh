#!/usr/bin/env bash

PID_FILE=/tmp/defcon.lock

if [ -f /tmp/defcon.lock ]; then
  kill -9 "$(<$PID_FILE)" && rm -f $PID_FILE
else
  mpv http://somafm.com/defcon256.pls &
  echo $! > $PID_FILE
fi
