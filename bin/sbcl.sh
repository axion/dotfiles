#!/bin/sh

case $(hostname) in
  wyvern)
    HEAP_SIZE=32768
    ;;
  *)
    HEAP_SIZE=8192
    ;;
esac

sbcl --core ~/.data/sbcl/sbcl.core --noinform --dynamic-space-size $HEAP_SIZE
