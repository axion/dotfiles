#!/usr/bin/env bash

RAND=$(openssl rand -base64 100 | tr -dc '[:alpha:]\n' | head -c6)
LOCAL_PATH=/tmp
LOCAL_FILE=screenshot.png
REMOTE_HOST=beholder
REMOTE_PATH=/srv/http/i.lisp.cl
REMOTE_FILE=$RAND.png
URL_PATH="https://i.lisp.cl"
LOG_FILE="/tmp/screenshot.log"

rm -f ${LOCAL_PATH}/${LOCAL_FILE}
xsel -p -c
xsel -b -c
sleep 0.5
scrot -s ${LOCAL_PATH}/${LOCAL_FILE} &>> "${LOG_FILE}" &&
  scp "${LOCAL_PATH}/${LOCAL_FILE}" "${REMOTE_HOST}:${REMOTE_PATH}/${REMOTE_FILE}" &>> "$LOG_FILE"
rm -f ${LOCAL_PATH}/${LOCAL_FILE}
echo "${URL_PATH}/${REMOTE_FILE}" | xclip -selection primary
echo "${URL_PATH}/${REMOTE_FILE}" | xclip -selection clipboard
echo "${URL_PATH}/${REMOTE_FILE}"
