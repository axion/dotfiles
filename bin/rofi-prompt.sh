#!/bin/sh

[ "$(printf "No\\nYes" | rofi -dmenu -i -p ">" -mesg "$1")" = "Yes" ] && $2
