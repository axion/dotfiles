#!/bin/sh

for file in ~/.tmuxinator/*.yml; do
  tmuxinator start "$(basename "${file%.*}")"
done
