#!/usr/bin/env bash
#
# ~/.bash_profile
#
# This file is sourced by Bash for login shells. Because this file exists, ~/.profile will not be
# sourced, so we will manually do so if one exists in this file. This file is for setting up the
# environment when Bash is a login shell.

# ~/.profile isn't sourced by Bash if this file exists, so we do so manually to bring in the general
# login shell settings common to all shell variants.
[[ -r "$HOME/.profile" ]] && source "$HOME/.profile"

# ~/.bashrc isn't sourced when the login shell is interactive, so we do so manually to set up our
# interactive login shell environment.
[[ $- = *i* && -r "$HOME/.bashrc" ]] && source "$HOME/.bashrc"
